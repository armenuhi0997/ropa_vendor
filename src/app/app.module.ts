import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieService } from 'ngx-cookie-service';
//import { CookieService, CookieOptions, BaseCookieOptions } from 'angular2-cookie/core';
import { ApiService, AppService } from './com/annaniks/ropa/sevices';
import { AuthGuardDeliveryService } from './com/annaniks/ropa/sevices/authguard-delivery';
import { httParams, mediaParams } from 'src/config';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [
    { provide: 'BASE_URL', useValue: httParams.baseUrl },
    { provide: 'FILE_URL', useValue: mediaParams.fileUrl },
    // { provide: CookieOptions, useClass: BaseCookieOptions },
    // { provide: CookieOptions, useValue: {} },
    ApiService,
    CookieService,
    AuthGuardDeliveryService,
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }