import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardDeliveryService } from './com/annaniks/ropa/sevices/authguard-delivery';

let appRoutes: Routes = [
    { path: '', redirectTo: 'vendor', pathMatch: 'full' },
    { path: 'vendor', loadChildren: './com/annaniks/ropa/views/delivary/delivary.module#DelivaryModule', canActivate: [AuthGuardDeliveryService] },
    { path: 'login/vendor', loadChildren: './com/annaniks/ropa/views/login-vendor/login-vendor.module#LoginVendorModule' },
    { path: 'registration', loadChildren: './com/annaniks/ropa/views/login-vendor/registration/registration.module#RegistrationModule' }
]
@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }