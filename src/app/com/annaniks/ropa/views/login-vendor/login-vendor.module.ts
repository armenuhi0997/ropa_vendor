import { NgModule } from "@angular/core";
import { LoginVendorView } from "./login-vendor.view";
import { SharedModule } from "../../shared/shared.module";
import { LoginVendorRoutingModule } from "./login-vendor.routing.module";
import { LoginVendorService } from "./login-vendor.service";

@NgModule({
    declarations: [LoginVendorView],
    imports: [LoginVendorRoutingModule, SharedModule],
    providers:[LoginVendorService]
})
export class LoginVendorModule { }