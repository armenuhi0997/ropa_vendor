import { Component } from "@angular/core";
import { LoginVendorService } from "./login-vendor.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
    selector: 'login-vendor',
    templateUrl: 'login-vendor.view.html',
    styleUrls: ['login-vendor.view.scss']
})
export class LoginVendorView {
    public visiblity: boolean = false
    public formGroup: FormGroup;
    public errorMessage: string;
    constructor(
        private _loginVendorService: LoginVendorService,
        private _formBuilder: FormBuilder,
        private _router: Router) {
        this._validation()
    }
    private _validation():void {
        this.formGroup = this._formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$/)]],
            password: ['', Validators.required],
        })
    }
    public login():void{
        if (this.formGroup.valid) {
            this.errorMessage = ''
            this.visiblity = true
            this._loginVendorService.loginDelivary(this.formGroup.value.email,
                this.formGroup.value.password)
                .subscribe(() => {},
                    () => {
                        this.visiblity = false
                        this.errorMessage = "Invalid email or password"
                    },
                    () => {
                        this.visiblity = false;
                        this._router.navigate(['/vendor'])
                    }
                )
        } else {
            if (this.formGroup.get('email').hasError('pattern')) {
                this.errorMessage = 'Please enter a valid email'
            } else {
                this.errorMessage = 'Please fill all fields'
            }
        }
    }
    public goToRegistrationPage():void {
        this._router.navigate(['/registration'])
    }
}