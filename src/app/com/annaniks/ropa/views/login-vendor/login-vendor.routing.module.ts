import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginVendorView } from "./login-vendor.view";
let loginVendor: Routes = [{ path: '', component: LoginVendorView }]
@NgModule({
    imports: [RouterModule.forChild(loginVendor)],
    exports:[RouterModule]
})
export class LoginVendorRoutingModule { }