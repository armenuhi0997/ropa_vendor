import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable()
export class RegistrationService{
    constructor(
        private _httpClient: HttpClient,
        @Inject('BASE_URL') private _baseUrl: string,
    ) { }
    /**
     * 
     * @param login 
     * @param password 
     * @param email 
     */
    public registrationVendor(login: string, password: string,email:string) {
        return this._httpClient.post(this._baseUrl + 'delivery/registered', 
        { login: login, password: password,email:email }).pipe(
            map((data: { token: string }) => {              
                return data
            })
        )
    }
}
