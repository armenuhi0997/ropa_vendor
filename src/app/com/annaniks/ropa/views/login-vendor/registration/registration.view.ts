import { Component } from "@angular/core";
import { Validators, FormGroup, FormBuilder, AbstractControl } from "@angular/forms";
import { Router } from "@angular/router";
import { RegistrationService } from "./registration.service";
import { MessageService } from "primeng/components/common/messageservice";

@Component({
    selector: 'registration',
    templateUrl: 'registration.view.html',
    styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    styleUrls: ['registration.view.scss'],
    providers: [MessageService]
})
export class RegistrationView {
    public errorMessage:string;
    public formGroup: FormGroup;
    public visiblity: boolean = false;
    public validationArray = {
        name: [
            { type: 'required', text: 'Name is required' }
        ],
        email: [
            { type: 'required', text: 'Email is required' },
            { type: 'email', text: 'Enter a valid email' }
        ],
        password: [
            { type: 'required', text: 'Password is required' }
        ],
    }
    constructor(private _formBuilder: FormBuilder,private messageService: MessageService, private _router: Router,private _registrService:RegistrationService) {
        this._validation()
    }
    private _showSuccess():void {        
        this.messageService.add({key:'registrKey',severity:'success', detail: 'Registration successful'});
    }
   private _showError() {
        this.messageService.add({key:'error',severity:'error',  detail:'This Email or Brand / Shop Name are exist'});
    }
    public back():void {
        this._router.navigate(['/login/vendor'])
    }
    private _validation():void {
        this.formGroup = this._formBuilder.group({
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$/)]],
            password: ['', Validators.required],
            rePassword:['',Validators.required],            
        },
        {validator:this.paswordConfirming})
    }
    /**
     * 
     * @param c 
     */
    paswordConfirming(c:AbstractControl):{invalid:boolean}{
        if(c.get('password').value !== c.get('rePassword').value){
            return {invalid:true}
        }
    }
    public registration():void {        
        if (this.formGroup.valid) {
            this.visiblity = true;
            this._registrService.registrationVendor(this.formGroup.value.name,
                this.formGroup.value.password,this.formGroup.value.email).subscribe((data)=>{
                    this.visiblity=false;
                    this._showSuccess()
                    setTimeout(()=>{
                        this.back()
                    },4000)
                    
                },
                (error)=>{
                    this.visiblity=false
                    this._showError()
                    //this.errorMessage=error.error.message;                   
                })
          
        }
    }
}