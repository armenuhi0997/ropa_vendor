import { NgModule } from "@angular/core";
import { RegistrationView } from "./registration.view";
import { RegistrationRoutingModule } from "./registration.routing.module";
import { SharedModule } from "../../../shared/shared.module";
import { RegistrationService } from "./registration.service";


@NgModule({
    declarations: [RegistrationView],
    imports: [RegistrationRoutingModule, SharedModule],
    providers:[RegistrationService]
})
export class RegistrationModule { }