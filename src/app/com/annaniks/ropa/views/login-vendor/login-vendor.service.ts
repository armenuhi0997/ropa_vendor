import { Injectable, Inject } from "@angular/core";
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class LoginVendorService {
    constructor(private _httpClient: HttpClient,
        @Inject('BASE_URL') private _baseUrl: string, ) { }
    /**
     * 
     * @param email 
     * @param password 
     */
    public loginDelivary(email: string, password: string):Observable<{token:string}>  {
        return this._httpClient.post(this._baseUrl + 'delivery/login', { email: email, password: password }).pipe(
            map((data: { token: string }) => {
                localStorage.setItem('accessToken', data.token)
                localStorage.setItem('role', JSON.stringify('delivery'))
                return data
            })
        )
    }
}