import { Component } from "@angular/core";
import { AppService } from "../../sevices";

@Component({
    selector: 'delivary',
    templateUrl: 'delivary.view.html',
    styleUrls: ['delivary.view.scss']
})
export class DelivaryView {
    public isItem: boolean = true
    constructor(public appService: AppService) { }
}