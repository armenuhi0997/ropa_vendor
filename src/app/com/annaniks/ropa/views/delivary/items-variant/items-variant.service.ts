import { Injectable } from "@angular/core";
import { ApiService } from "../../../sevices";
import { Observable } from "rxjs";
import { ServerResponse, ItemsById } from "../../../types/interface";

@Injectable()
export class ItemsVariantService {
    constructor(private _apiService: ApiService) { }
    /**
     * 
     * @param id 
     */
    public getItemById(id: number): Observable<ServerResponse<ItemsById>> {
        return this._apiService.get('delivery/items/current/' + id)
    }
    /**
     * 
     * @param color 
     * @param itemId 
     * @param name 
     * @param price 
     * @param size 
     * @param stock 
     */
    public addVariant(color: string, itemId: number, name: string, price, size: string, stock) {
        return this._apiService.post('delivery/items/add/variant', {
            color: color,
            item: itemId,
            name: name,
            price: price,
            size: size,
            stock: stock
        })
    }

}