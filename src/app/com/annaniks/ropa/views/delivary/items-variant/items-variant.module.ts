import { NgModule } from "@angular/core";
import { ItemsVariantView } from "./items-variant.view";
import { ItemsVariantRoutingModule } from "./items-variant.routing.module";
import { SharedModule } from "../../../shared/shared.module";
import { ItemsVariantService } from "./items-variant.service";
import { AddVariantDialog } from "../../../dialogs";
import { ColorPickerModule } from 'ngx-color-picker'
@NgModule({
    declarations: [ItemsVariantView, AddVariantDialog],
    imports: [ItemsVariantRoutingModule, SharedModule,ColorPickerModule],
    entryComponents: [AddVariantDialog],
    providers: [ItemsVariantService]
})
export class ItemsVariantModule { }