import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ItemsVariantService } from "./items-variant.service";
import { ItemsById, ItemsVariant, ServerResponse } from "../../../types/interface";
import { MatDialog } from "@angular/material";
import { AddVariantDialog } from "../../../dialogs";
import { AppService } from "../../../sevices";


@Component({
    selector: 'items-variant',
    templateUrl: 'items-variant.view.html',
    styleUrls: ['items-variant.view.scss']
})
export class ItemsVariantView {
    public visiblity: boolean = true
    public id: number;
    public variants: ItemsVariant[] = [];
    public items: ItemsById;
    public isGet: boolean = false;
    public defaultImage: string;
    constructor(
        private _itemsVariantService: ItemsVariantService,
        private _activatedRoute: ActivatedRoute,
        private _matDialog: MatDialog,
        private _appService: AppService
    ) {
        this._activatedRoute.params.subscribe(params => {
            this.id = params['id'];
        })
    }

    ngOnInit() {
        this._getItemsVariant()
    }
    private _getItemsVariant():void {
        this._itemsVariantService.getItemById(this.id).subscribe((data: ServerResponse<ItemsById>) => {
            this.items = data.message;
            this.variants = this.items.itemVariant;
            this.visiblity = false
            this.isGet = true;
            for (let variant of this.items.itemVariant) {
                if (variant.color.startsWith('(')) {
                    let firstIndex = variant.color.indexOf('(');
                    let lastIndex = variant.color.indexOf(')');
                    if (variant.color.indexOf("'") > -1) {
                        let index1 = variant.color.indexOf("'");
                        variant.color = variant.color.substr(index1 + 1, lastIndex - 4)
                    } else {
                        variant.color = variant.color.substr(firstIndex + 1, lastIndex - 2);
                    }
                }
                if (variant.size.startsWith('(')) {
                    let firstIndex = variant.size.indexOf('(');
                    let lastIndex = variant.size.indexOf(')');
                    if (variant.size.indexOf("'") > -1) {
                        let index1 = variant.size.indexOf("'");
                        variant.size = variant.size.substr(index1 + 1, lastIndex - 4);
                    } else {
                        variant.size = variant.size.substr(firstIndex + 1, lastIndex - 2);
                    }
                }
            }
        })
    }
    public addVariant():void {
        let panelClass = this._appService.isOpen ? 'open-menu-modal' : 'close-menu-modal'
        let dialog = this._matDialog.open(AddVariantDialog,
            {
                height: '410px',
                width: '600px',
                data: this.id,
                panelClass: panelClass
            });
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                this.visiblity = true
                this._getItemsVariant()
            }
        })
    }

}