import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ItemsVariantView } from "./items-variant.view";
let itemsVariantRoutes:Routes=[{path:'',component:ItemsVariantView}]
@NgModule({
    imports:[RouterModule.forChild(itemsVariantRoutes)],
    exports:[RouterModule]
})
export class ItemsVariantRoutingModule{}