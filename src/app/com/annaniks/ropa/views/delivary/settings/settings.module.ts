import { NgModule } from "@angular/core";
import { SettingsView } from "./settings.view";
import { SettingsRoutingModule } from "./settings.routing.module";
import { SharedModule } from "../../../shared/shared.module";
import { SettingsService } from "./settings.service";

@NgModule({
    declarations:[SettingsView],
    imports:[SettingsRoutingModule,SharedModule],
    providers:[SettingsService]
})
export class SettingsModule{}