import { Injectable } from "@angular/core";
import { ApiService } from "../../../sevices";
import { Observable } from "rxjs";

import { VendorInfo, ServerResponse } from "../../../types/interface";

@Injectable()
export class SettingsService {
    constructor(private _apiService: ApiService) { }
    public getDelivery():Observable<ServerResponse<VendorInfo>> {
        return this._apiService.get('delivery/me')
    }
    /**
     * 
     * @param email 
     * @param login 
     * @param password 
     */
    public changeDeliveryInfo(email:string,login:string,password:string) {
        return this._apiService.put('delivery/me', {
            email:email,           
            login: login,
            password:password
        })
    }
}