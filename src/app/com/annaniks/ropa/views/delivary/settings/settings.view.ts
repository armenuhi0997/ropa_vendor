import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SettingsService } from "./settings.service";
import { VendorInfo, ServerResponse } from "../../../types/interface";

@Component({
    selector: 'settings',
    templateUrl: 'settings.view.html',
    styleUrls: ['settings.view.scss']
})
export class SettingsView {
    public visiblity: boolean;
    public formGroup: FormGroup;
    public vendor: VendorInfo
    constructor(private _formBuilder: FormBuilder, private _settingsService: SettingsService) {
        this._validation()
    }
    ngOnInit() {
        this._getDelivery()
    }
    private _getDelivery():void {
        this.visiblity = true
        this._settingsService.getDelivery().subscribe((data:ServerResponse<VendorInfo>) => {
            this.vendor = data.message
            this._setValue()
        })
    }
    private _validation():void {
        this.formGroup = this._formBuilder.group({
            login: ['', Validators.required],
            password: [''],
            email: ['', [Validators.required, Validators.email]],
           // code: ['', Validators.required]
        })
    }
    private _setValue():void {
        this.formGroup.get('login').setValue(this.vendor.login);
        this.formGroup.get('email').setValue(this.vendor.email);
       // this.formGroup.get('code').setValue(this.vendor.code);
        this.visiblity = false
    }

    public save():void {
        if (this.formGroup.valid) {
            this.visiblity = true
            this._settingsService.changeDeliveryInfo(this.formGroup.value.email,
                this.formGroup.value.login, this.formGroup.value.password).subscribe((data) => {
                    this._getDelivery()
                    this.visiblity = false
                })
        }
        //d67613033e6b42368f2d37d63c30c15c:7bc9264bccd048da2980674836742862@ropatech
    }
}