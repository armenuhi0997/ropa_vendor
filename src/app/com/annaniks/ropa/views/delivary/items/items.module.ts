import { NgModule } from "@angular/core";
import { ItemsView } from "./items.view";
import { ItemsRouterModule } from "./items.routing.module";
import { SharedModule } from "../../../shared/shared.module";
import { ItemsService } from "./items.service";
import { AddItemsDialog } from "../../../dialogs/add-items/add-items.dialog";

@NgModule({
    declarations:[ItemsView,AddItemsDialog],
    imports:[ItemsRouterModule,SharedModule],
    entryComponents:[AddItemsDialog],
    providers:[ItemsService]
})
export class ItemsModule{}