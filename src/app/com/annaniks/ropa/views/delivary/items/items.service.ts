import { Injectable } from "@angular/core";
import { ApiService } from "../../../sevices";
import { Observable } from "rxjs";
import { ServerResponse, Brand, Country, Category, Item, ItemsById } from "../../../types/interface";

@Injectable()
export class ItemsService {
    constructor(private _apiService: ApiService) { }
    public getBrands():Observable<ServerResponse<Brand[]>> {
        return this._apiService.get('delivery/brands')
    }
    public getCountries():Observable<ServerResponse<Country[]>> {
        return this._apiService.get('delivery/countries')
    }
    public getCategory():Observable<ServerResponse<Category[]>> {
        return this._apiService.get('delivery/categories')
    }
    /**
     * 
     * @param name 
     * @param description 
     * @param brandId 
     * @param categoryId 
     * @param manufacturerCode 
     * @param vendorCode 
     * @param country 
     * @param season 
     */
    public addItems(name: string,
        description: string,
        brandId: string,
        categoryId: string,
        manufacturerCode: string,
        vendorCode: string,
        country: string,
        season: string){
        return this._apiService.post('delivery/items/add', {
            name: name,
            description: description,
            brands: brandId,
            category: categoryId,
            vender_code: vendorCode,
            manufacturer_code: manufacturerCode,
            country_of_origin: country,
            season: season
        })
    }
    public getItems():Observable<ServerResponse<Item[]>>  {
        return this._apiService.get('delivery/me/items')
    }
    /**
     * 
     * @param id 
     */
    public getItemsVariant(id: number):Observable<ServerResponse<ItemsById>> {
        return this._apiService.get('delivery/items/current/' + id)
    }
    /**
     * 
     * @param itemId 
     * @param formData 
     */
    public addItemImage(itemId: number, formData: FormData) {
        return this._apiService.putFormData(`delivery/items/image/${itemId}`, formData, 'response', 'text')
    }
}