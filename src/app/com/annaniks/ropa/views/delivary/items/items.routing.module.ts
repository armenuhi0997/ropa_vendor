import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ItemsView } from "./items.view";
let itemsRoutes:Routes=[
    {path:'',component:ItemsView},
    {path:':id/variants',loadChildren:'../items-variant/items-variant.module#ItemsVariantModule'}
]
@NgModule({
    imports:[RouterModule.forChild(itemsRoutes)],
    exports:[RouterModule]
})
export class ItemsRouterModule{
    
}