import { Component, Inject } from "@angular/core";
import { ItemsService } from "./items.service";
import { MatDialog } from "@angular/material";
import { AddItemsDialog } from "../../../dialogs/add-items/add-items.dialog";
import { Item, ServerResponse } from "../../../types/interface";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { AppService } from "../../../sevices";
import { Observable } from "rxjs";

@Component({
    selector: 'items',
    templateUrl: 'items.view.html',
    styleUrls: ['items.view.scss']
})
export class ItemsView {
    public visiblity: boolean = true
    public items: Item[] = []
    constructor(
        private _itemsService: ItemsService,
        private _matDialog: MatDialog,
        private _router: Router,
        private _appService: AppService,
        @Inject('FILE_URL') public fileUrl: string,
    ) { }
    ngOnInit() {
        this._getItems().subscribe(() => this.visiblity = false)
    }
    /**
     * 
     * @param item 
     */
    public setImage(item: Item): object {
        let style = {}
        if (item.images[0].image.startsWith('https://')) {
            style['background-image'] = "url(" + item.images[0].image + ")"
        } else {
            style['background-image'] = "url(" + this.fileUrl + item.images[0].image + ")"
        }
        return style
    }

    public addItem(): void {
        let panelClass = this._appService.isOpen ? 'open-menu-modal' : 'close-menu-modal'
        let dialog = this._matDialog.open(AddItemsDialog, {
            height: '700px',
            width: '600px',
            panelClass: panelClass
        })
        dialog.afterClosed().subscribe((data) => {
            if (data) {
                this.visiblity = true
                this._getItems().subscribe(() => this.visiblity = false)
            }
        })
    }
    /**
     * 
     * @param id 
     */
    public openView(id: number): void {
         this._router.navigate([`/vendor/items/${id}/variants/`])
    }
    private _getItems(): Observable<ServerResponse<Item[]>> {
        return this._itemsService.getItems().pipe(
            map((data: ServerResponse<Item[]>) => {
                this.items = data.message;
                return data
            })
        )
    }

}