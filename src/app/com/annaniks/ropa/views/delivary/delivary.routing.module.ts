import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DelivaryView } from "./delivary.view";
let delivaryRoutes: Routes = [{
    path: '', component: DelivaryView,
    children: [
        {path:'',pathMatch:'full',redirectTo:'items'},
        {path:'items',loadChildren:'./items/items.module#ItemsModule'},
        {path:'shopify-panel',loadChildren:'./shopify-panel/shopify-panel.module#ShopifyPanelModule'},
        {path:'settings',loadChildren:'./settings/settings.module#SettingsModule'}
        
    ]
}]
@NgModule({
    imports: [RouterModule.forChild(delivaryRoutes)],
    exports: [RouterModule]
})
export class DelivaryRoutingModule { }