import { Component, Inject } from "@angular/core";
import { ShopifyService } from "./shopify-panel.service";
import { map } from "rxjs/operators";
import {
    Country, Brand, Category, ShopifyProduct,
    SelectProduct, VendorInfo, Product, ServerResponse
} from "../../../types/interface";
import { Subscription, forkJoin, Observable } from "rxjs";
import { MatDialog } from "@angular/material";
import { AddApiKeyDialog } from "../../../dialogs";
import { AppService } from "../../../sevices";

@Component({
    selector: 'shopify-panel',
    templateUrl: 'shopify-panel.view.html',
    styleUrls: ['shopify-panel.view.scss']
})
export class ShopifyPanelView {
    public visiblity: boolean;
    public api_key: string;
    public isGet: boolean = false
    public shopifyProducts: ShopifyProduct[] = []
    public categories: Category[];
    public brands: Brand[];
    public countries: Country[];
    private _info: VendorInfo;
    private _subscription: Subscription;
    constructor(private _shopifyService: ShopifyService,
        @Inject('FILE_URL') public fileUrl: string,
        private _matDialog: MatDialog,
        private _appService: AppService
    ) { }
    ngOnInit() {
        this.combineObservable()
    }
    combineObservable() {
        this.visiblity = true
        const combine = forkJoin(
            this._getBrands(),
            this._getCategory(),
            this._getShopfy(),
            this._getCountries(),
            this._getDelivery()
        )
        this._subscription = combine.subscribe(() => {
            this.isGet = true;
            this.visiblity = false
        })
    }
    combineOtherObservableTwo():void {
        this.visiblity = true
        const combine = forkJoin(
            this._getShopfy(),
            this._getDelivery()
        )
        this._subscription = combine.subscribe(() => {
            this.isGet = true
            this.visiblity = false
        })
    }
    private _getDelivery():Observable<ServerResponse<VendorInfo>> {
        return this._shopifyService.getDelivery().pipe(
            map((data: ServerResponse<VendorInfo>) => {
                this._info = data.message
                this.api_key = this._info.code
                return data
            })
        )
    }
    private _getShopfy():Observable<ServerResponse<ShopifyProduct[]>> {
        return this._shopifyService.getShopfy().pipe(
            map((data: ServerResponse<ShopifyProduct[]>) => {
                this.shopifyProducts = data.message;
                for (let product of this.shopifyProducts) {
                    product['selectedBrand'] = '';
                    product['selectCategories'] = '';
                    product['selectedCountry'] = '';
                    product['manufacturerCode'] = '';
                    product['season'] = '';
                    product['selectedColor'] = '';
                    product['selectedSize'] = '';
                    product['vendorCode'] = '';
                }
                return data
            })
        )
    }
    private _getCategory():Observable<ServerResponse<Category[]>> {
        return this._shopifyService.getCategory().pipe(
            map((data: ServerResponse<Category[]>) => {
                this.categories = data.message
                return data
            })
        )
    }
    private _getBrands():Observable<ServerResponse<Brand[]>> {
        return this._shopifyService.getBrands().pipe(
            map((data: ServerResponse<Brand[]>) => {
                this.brands = data.message
                return data
            })
        )
    }
    private _getCountries():Observable<ServerResponse<Country[]>> {
        return this._shopifyService.getCountries().pipe(
            map((data: ServerResponse<Country[]>) => {
                this.countries = data.message
                return data
            })
        )
    }
    /**
     * 
     * @param product 
     */
    public addProduct(product: ShopifyProduct):void {
        if (product.selectedBrand && product.selectCategories &&
            product.selectedCountry && product.selectedSize && product.selectedColor &&
            product.manufacturerCode && product.season) {
            this.visiblity = true
            let colorObj: SelectProduct = product.options.filter((data) => {
                return data.id == product.selectedColor
            })[0]
            let color_key = colorObj ? 'option' + colorObj.position : 'None';
            let sizeObj = product.options.filter((data) => {
                return data.id == product.selectedSize
            })[0]
            let size_key = sizeObj ? 'option' + sizeObj.position : 'None';
            let item: Product = {
                name: product.title,
                description: product.body_html,
                shopify_id: product.id,
                images: product.images,
                brand: product.selectedBrand,
                category: product.selectCategories,
                country: product.selectedCountry,
                manufacturer_code: product.manufacturerCode,
                vender_code: 0,//product.vendorCode,
                season: product.season,
                color_key: color_key,
                size_key: size_key,
                variants: []
            }
            for (let variant of product.variants) {
                item.variants.push({
                    name: variant.title,
                    stock: variant.inventory_quantity,
                    price: variant.price,
                    color: item.color_key == 'None' ? 'None' : variant[item.color_key],
                    size: item.size_key == 'None' ? 'None' : variant[item.size_key],
                    shopify_id: variant.product_id,
                    shopify_vendor_id: variant.id
                })
            }
            this._shopifyService.sendProduct(item).subscribe((data) => {
                this._getShopfy().subscribe(() => this.visiblity = false)
            })
        }
    }
    public addAppiKey():void {
        let panelClass = this._appService.isOpen ? 'open-menu-modal' : 'close-menu-modal'
        let dialog = this._matDialog.open(AddApiKeyDialog, {
            height: '245px',
            width: '350px',
            data: this._info,
            panelClass: panelClass
        })
        dialog.afterClosed().subscribe((result) => {
            if (result) {
                this.isGet = false
                this.combineOtherObservableTwo()
            }
        })
    }
    ngOnDestroy() {
        this._subscription.unsubscribe()
    }
}