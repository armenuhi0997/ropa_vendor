import { NgModule } from "@angular/core";
import { ShopifyPanelRoutingModule } from "./shopify-panel.routing.module";
import { SharedModule } from "../../../shared/shared.module";
import { ShopifyService } from "./shopify-panel.service";
import { ShopifyPanelView } from "./shopify-panel.view";
import { EditProductVariantDialog, AddApiKeyDialog } from "../../../dialogs";

@NgModule({
    declarations:[ShopifyPanelView,EditProductVariantDialog,AddApiKeyDialog],
    imports:[SharedModule,ShopifyPanelRoutingModule],
    entryComponents:[EditProductVariantDialog,AddApiKeyDialog],
    providers:[ShopifyService]
})
export class ShopifyPanelModule{}