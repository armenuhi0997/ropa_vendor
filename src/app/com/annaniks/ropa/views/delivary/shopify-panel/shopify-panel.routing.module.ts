import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ShopifyPanelView } from "./shopify-panel.view";
let shopifyRoutes: Routes = [{ path: '', component: ShopifyPanelView }]
@NgModule({
    imports: [RouterModule.forChild(shopifyRoutes)]
})
export class ShopifyPanelRoutingModule { }