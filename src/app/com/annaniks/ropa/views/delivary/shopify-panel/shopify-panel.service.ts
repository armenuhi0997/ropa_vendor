import { ApiService } from "../../../sevices";
import { Injectable } from "@angular/core";
import { Product, ServerResponse, ShopifyProduct, Brand, Country, Category, VendorInfo } from "../../../types/interface";
import { Observable } from "rxjs";

@Injectable()
export class ShopifyService {
    constructor(private _apiService: ApiService) { }
    public getShopfy(): Observable<ServerResponse<ShopifyProduct[]>> {
        return this._apiService.get('delivery/shopify')
    }
    public getBrands():Observable<ServerResponse<Brand[]>> {
        return this._apiService.get('delivery/brands')
    }
    public getCountries():Observable<ServerResponse<Country[]>> {
        return this._apiService.get('delivery/countries')
    }
    public getCategory():Observable<ServerResponse<Category[]>>{
        return this._apiService.get('delivery/categories')
    }
    /**
     * 
     * @param item 
     */
    public sendProduct(item: Product) {
        return this._apiService.post('delivery/shopify/set', item)
    }
    public getDelivery():Observable<ServerResponse<VendorInfo>> {
        return this._apiService.get('delivery/me')
    }
    /**
     * 
     * @param code 
     * @param email 
     * @param login 
     * @param password 
     */
    public changeDeliveryInfo(code: string, email: string, login: string, password?: string) {
        return this._apiService.put('delivery/me', {
            code: code,
            email: email,
            login: login,
            password: password
        })
    }
}