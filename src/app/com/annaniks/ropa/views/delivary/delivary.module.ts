import { NgModule } from "@angular/core";
import { DelivaryView } from "./delivary.view";
import { DelivaryRoutingModule } from "./delivary.routing.module";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
    declarations:[DelivaryView],
    imports:[DelivaryRoutingModule,SharedModule],
})
export class DelivaryModule{
    
}