import { Injectable } from "@angular/core";
import { MenuTypes } from "../types/interface";

@Injectable()
export class MenuService {
    public menuPages: MenuTypes[] = [];
    public delivaryArr: MenuTypes[] = [
        {
            pageName: 'Items',
            path: '/vendor/items'
        },
        {
            pageName:'Shopify panel',
            path:'/vendor/shopify-panel'
        },
        {
            pageName:'Settings',
            path:'/vendor/settings'
        },
        {
            pageName: 'Log out',
            path: '/login/vendor'
        },
    ]
    constructor() { }
    ngOnInit() { }
    public getDelivaryList() {
        return this.delivaryArr
    }

}