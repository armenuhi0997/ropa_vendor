import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()

export class ApiService {
    constructor(
        private _httpClient: HttpClient,
        @Inject('BASE_URL') private _baseUrl: string
    ) { }
    /**
       * 
       * @param url - request url
       * @param observe - httpOption for get full response,not required
       * @param responseType - httpOption for get full response on type text
       */
    public get(url: string, observe?:string, responseType?:string):Observable<Object | any> {
        let accessToken = localStorage.getItem('accessToken') || ''
        //this._cookieService.get('accessToken') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.get(this._baseUrl + url, params)
    }
    /**
     * 
     * @param url - request url, 
     * @param body - sending object
     * @param observe - httpOption for get full response
     * @param responseType 
     */
    public post(url: string, body: object, observe?:string, responseType?:string):Observable<Object | any> {
        let accessToken = localStorage.getItem('accessToken') || ''
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.post(this._baseUrl + url, body, params);
    }
    /**
     * 
     * @param url 
     * @param formData 
     * @param observe 
     * @param responseType 
     */
    public postFormData(url: string, formData: FormData, observe?:string, responseType?:string):Observable<Object | any> {
        let accessToken = localStorage.getItem('accessToken') || '';
        let headers = new HttpHeaders({
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.post(this._baseUrl + url, formData, params);
    }
    /**
     * 
     * @param url 
     * @param formData 
     * @param observe 
     * @param responseType 
     */
    public putFormData(url: string, formData: FormData, observe?:string, responseType?:string):Observable<Object | any> {
        let accessToken = localStorage.getItem('accessToken') || ''
        let headers = new HttpHeaders({
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.put(this._baseUrl + url, formData, params);
    }

    /**
     * 
     * @param url 
     * @param body 
     * @param observe 
     * @param responseType 
     */
    public put(url: string, body: object, observe?:string, responseType?:string):Observable<Object | any> {
        let accessToken = localStorage.getItem('accessToken') || ''
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.put(this._baseUrl + url, body, params);
    }
    /**
     * 
     * @param url 
     * @param observe 
     * @param responseType 
     */
    public delete(url: string, observe?:string, responseType?:string):Observable<Object | any> {
        let accessToken = localStorage.getItem('accessToken') || ''
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.delete(this._baseUrl + url, params);
    }
}