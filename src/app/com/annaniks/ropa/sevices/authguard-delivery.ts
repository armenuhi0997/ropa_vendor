import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";


@Injectable()
export class AuthGuardDeliveryService implements CanActivate {
    constructor(private _router: Router) { }
    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        if (localStorage.getItem('accessToken')) {
            return true
        } else {
            this._router.navigate(['/login/vendor'])
            return false
        }
    }
}