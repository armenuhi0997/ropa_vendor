import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../sevices';

@Component({
    selector: 'app-loading',
    templateUrl: 'loading.component.html',
    styleUrls: ['loading.component.scss']
})
export class LoadingComponent implements OnInit {
    @Input('visiblity') visiblity: boolean = true;
    constructor(public appService: AppService) { }
    ngOnInit() { }

}