import { Component } from "@angular/core";
import { AppService } from "../../sevices";

@Component({
    selector: 'top-bar',
    templateUrl: 'top-bar.component.html',
    styleUrls: ['top-bar.component.scss']
})
export class TopBarComponent {
    constructor(public appService: AppService) { }
    openOrCloseMenu() {
        this.appService.isOpen = !this.appService.isOpen
    }
    
}