import { Component } from '@angular/core';
import { MenuService } from '../../sevices/menu.service';
import { AppService } from '../../sevices';
import { Router } from '@angular/router';
import { MenuTypes } from '../../types/interface';

@Component({
    selector: 'left-menu',
    templateUrl: 'left-menu.component.html',
    styleUrls: ['left-menu.component.scss']
})
export class LeftMenuComponent {
    public menuList: MenuTypes[] = [];
    constructor(
        private _menuService: MenuService,
        public appService: AppService,
        private _router: Router,

    ) {
        this.menuList = this._menuService.getDelivaryList()
    }
    /**
     * 
     * @param page 
     */
    public openPage(page: MenuTypes) {
        this._router.navigate([page.path])
        if (page.path == '/login/vendor') {
            localStorage.removeItem('role')
            localStorage.removeItem('accessToken')
        }
    }
}