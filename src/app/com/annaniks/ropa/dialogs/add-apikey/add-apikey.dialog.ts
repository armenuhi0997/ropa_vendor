import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { ShopifyService } from "../../views/delivary/shopify-panel/shopify-panel.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
    selector: 'api-key',
    templateUrl: 'add-apikey.dialog.html',
    styleUrls: ['add-apikey.dialog.scss']
})
export class AddApiKeyDialog {
    public visiblity: boolean = false
    public formGroup: FormGroup
    constructor(public dialogRef: MatDialogRef<AddApiKeyDialog>, private _formBuilder: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any, private _shopifyService: ShopifyService
    ) {
        this._validate()
    }
    private _validate():void {
        this.formGroup = this._formBuilder.group({
            key: ['', Validators.required],
            shop_name: ['', Validators.required]
        })
        this._setValue()
    }
    private _setValue():void {
        if (this.data.code) {
            let index = this.data.code.indexOf('@');
            this.formGroup.get('key').setValue(this.data.code.substr(0, index))
            this.formGroup.get('shop_name').setValue(this.data.code.substr(index + 1))
        }
    }
    close():void {
        this.dialogRef.close()
    }
    add():void {
        if (this.formGroup.valid) {
            this.visiblity = true
            this._shopifyService.changeDeliveryInfo(this.formGroup.value.key + '@' + this.formGroup.value.shop_name, this.data.email, this.data.login, '').subscribe((data) => {
                this.visiblity = false;
                this.dialogRef.close(true)
            })
        }
    }
}