import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormBuilder } from "@angular/forms";

@Component({
    selector: 'edit-product',
    templateUrl: 'edit-product-variant.dialog.html',
    styleUrls: ['edit-product-variant.dialog.scss']
})
export class EditProductVariantDialog {
    public visiblity: boolean;
    constructor(public dialogRef: MatDialogRef<EditProductVariantDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any, private _formBuilder: FormBuilder, ) {
    }
    ngOnInit() { }
    public close():void {
        this.dialogRef.close()
    }
}