import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ItemsVariantService } from "../../views/delivary/items-variant/items-variant.service";

@Component({
    selector: 'add-variant',
    templateUrl: 'add-variant.dialog.html',
    styleUrls: ['add-variant.dialog.scss']
})
export class AddVariantDialog {
    public formGroup: FormGroup;
    public visiblity: boolean = false;
    // public color2: string = '#d4d0c8';
    constructor(public dialogRef: MatDialogRef<AddVariantDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any, private _formBuilder: FormBuilder,
        private _itemsVariantService: ItemsVariantService) {
    }
    ngOnInit() {
        this._validation()
    }
    public close():void {
        this.dialogRef.close()
    }
    private _validation():void {
        this.formGroup = this._formBuilder.group({
            name: ['', Validators.required],
            price: ['', Validators.required],
            size: ['', Validators.required],
            stock: ['', Validators.required],
            color: ['', Validators.required],
        })
    }
    public addVariant():void {
        if (this.formGroup.valid) {
            this.visiblity = true
            this._itemsVariantService.addVariant(this.formGroup.value.color, this.data, this.formGroup.value.name,
                this.formGroup.value.price, this.formGroup.value.size, this.formGroup.value.stock).subscribe((data) => {
                    this.visiblity = false
                    this.dialogRef.close(true)
                })
        }
    }
}