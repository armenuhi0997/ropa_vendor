import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ItemsService } from "../../views/delivary/items/items.service";
import { Subscription, forkJoin, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Brand, Category, Item, Country, ImageEvent, ServerResponse } from "../../types/interface";

@Component({
    selector: 'add-items',
    templateUrl: 'add-items.dialog.html',
    styleUrls: ['add-items.dialog.scss']
})
export class AddItemsDialog {
    public isGetValue: boolean = false
    private _subscription: Subscription;
    public errorMessage: string;
    public formGroup: FormGroup;
    public isGet: boolean = false;
    public brands: Brand[] = [];
    public visiblity: boolean = true
    public categories: Category[] = [];
    public countries: Country[] = [];
    public defaulImageArray: Array<string> = [];
    private _images: ImageEvent[] = [];
    constructor(public dialogRef: MatDialogRef<AddItemsDialog>,
        @Inject(MAT_DIALOG_DATA) public data: Item,
        private _formBuilder: FormBuilder,
        private _itemsService: ItemsService) {
        this._combineObservable()
    }
    ngOnInit() {
        this._validation();
    }
    private _setValue():void {
        if (this.data) {
            this.formGroup.get('name').setValue(this.data.name)
            this.formGroup.get('description').setValue(this.data.description)
            this.formGroup.get('vendorCode').setValue(this.data.vender_code)
            this.formGroup.get('manifacturerCode').setValue(this.data.manufacturer_code)
            this.formGroup.get('country').setValue(this.data.country_of_origin)
            this.formGroup.get('season').setValue(this.data.season)
            this.formGroup.get('selectBrands').setValue(this.data.brands_name);
            this.formGroup.get('selectCategories').setValue(this.data.category_name);
            this._getItemsVariant()
        }
    }
    private _combineObservable() {
        const combine = forkJoin(
            this._getBrands(),
            this._getCategory(),
            this._getCountries()
        )
        this._subscription = combine.subscribe((data) => {
            this.isGet = true;
            this.visiblity = false
        })
    }
    private _validation():void {
        this.formGroup = this._formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.required],
            vendorCode: ['', Validators.required],
            manifacturerCode: ['', Validators.required],
            country: ['', Validators.required],
            season: ['', Validators.required],
            selectBrands: ['', Validators.required],
            selectCategories: ['', Validators.required]
        })
        this._setValue()
    }
    private _getItemsVariant():void {
        this._itemsService.getItemsVariant(this.data.id).subscribe((data) => {
        })
    }
    public changeImage(event):void {
        if (event) {
            let files = event.target.files;
            if (files.length > 0) {
                this._images.push({ file: files[0], name: files[0].name });
                let reader = new FileReader();
                reader.onload = (e: any) => {
                    this.defaulImageArray.push('url(' + e.target.result + ')');
                }
                reader.readAsDataURL(files[0]);
            }
            event.preventDefault();
        }
    }
    /**
     * 
     * @param i 
     */
    public delete(i: number):void {
        this.defaulImageArray.splice(i, 1);
        this._images.splice(i, 1)
    }
    /**
     * 
     * @param id 
     */
    public addImage(id: number):void {
        let formData: FormData = new FormData();
        for (let i = 0; i < this._images.length; i++) {
            formData.append('file' + (i + 1).toString(), this._images[i].file, this._images[i].name);
        }
        this._itemsService.addItemImage(id, formData).subscribe((data) => {
            this.dialogRef.close(true);
            this.visiblity = false
        })
    }
    private _getCategory():Observable<ServerResponse<Category[]>> {
        return this._itemsService.getCategory().pipe(
            map((data: { message: Category[] }) => {
                this.categories = data.message
                return data
            })
        )
    }
    private _getBrands():Observable<ServerResponse<Brand[]>> {
        return this._itemsService.getBrands().pipe(
            map((data: { message: Brand[] }) => {
                this.brands = data.message
                return data
            })
        )
    }
    private _getCountries():Observable<ServerResponse<Country[]>> {
        return this._itemsService.getCountries().pipe(
            map((data: { message: Country[] }) => {
                this.countries = data.message
                return data
            })
        )
    }
    public close():void {
        this.dialogRef.close()
    }
    public addItem():void {
        if (this.formGroup.valid && this.defaulImageArray[0]) {
            this.errorMessage = ''
            this.visiblity = true
            this._itemsService.addItems(this.formGroup.value.name,
                this.formGroup.value.description,
                this.formGroup.value.selectBrands,
                this.formGroup.value.selectCategories,
                this.formGroup.value.manifacturerCode,
                this.formGroup.value.vendorCode,
                this.formGroup.value.country,
                this.formGroup.value.season
            ).subscribe((data: any) => {
                this.addImage(data.id)
            })
        } else {
            if (this.formGroup.valid && !this.defaulImageArray[0]) {
                this.errorMessage = 'Please add image'
            }
        }
    }
    ngOnDestroy() {
        this._subscription.unsubscribe()
    }
}

