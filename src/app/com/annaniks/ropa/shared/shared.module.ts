import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material';
import {
    LeftMenuComponent,
    TopBarComponent,
    LoadingComponent
} from '../components';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MenuService } from '../sevices';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ConfirmDialog } from '../dialogs';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import {ToastModule} from 'primeng/toast';
@NgModule({
    declarations: [
        LeftMenuComponent,
        TopBarComponent,
        LoadingComponent,
        ConfirmDialog,
    ],
    imports: [
        CommonModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MatInputModule,
        MatCheckboxModule,
        MatRadioModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatSlideToggleModule,
        ProgressSpinnerModule,
        ToastModule
    ],
    entryComponents: [
        ConfirmDialog,
    ],
    exports: [
        CommonModule,
        MatDialogModule,
        LeftMenuComponent,
        FormsModule,
        ReactiveFormsModule,
        TopBarComponent,
        RouterModule,
        MatInputModule,
        MatCheckboxModule,
        MatRadioModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        LoadingComponent,
        MatSlideToggleModule,
        ProgressSpinnerModule,
        ToastModule
    ],
    providers: [MenuService]
})
export class SharedModule { }