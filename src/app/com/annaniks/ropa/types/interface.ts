export interface ServerResponse<T> {
    message: T
}
export interface Brand {
    description: string
    id: number
    logo: null
    name: string
}
export interface Category {
    description: string
    id: number
    name: string
    picture: null
}
export interface Country {
    code: string
    continent_name: string
    id: number
    name: string
}
export interface Item {
    brands_name: any
    category_name: any
    country_of_origin: string
    description: string
    id: number
    manufacturer_code: string
    name: string
    season: string
    vender_code: string,
    images:Array<{image:string}>
}
export interface ItemsById {
    brands: Brand
    category: Category
    country_of_origin: string
    description: string
    id: number
    itemVariant: ItemsVariant[]
    manufacturer_code: string
    name: string
    season: string
    vender_code: string
}
export interface ItemsVariant {
    color: string
    id: number
    item_id: number
    name: string
    price: string
    size: string
    stock: string
}
export interface MenuTypes {
    pageName: string,
    path: string
}

export interface ImageEvent {
    file: File,
    name: string
}
export interface PostImage {
    id: number,
    path: string,
    post_id: number
}
export interface ShopifyProduct {
    admin_graphql_api_id: string
    body_html: string
    created_at: string
    handle: string
    id: number
    image: {
        admin_graphql_api_id: string
        alt: null
        created_at: string
        height: number
        id: number
        position: number
        product_id: number
        src: string
        updated_at: string
        variant_ids: []
        width: number
    }
    images: Array<{
        admin_graphql_api_id: string
        alt: null
        created_at: string
        height: number
        id: number
        position: number
        product_id: number
        src: string
        updated_at: string
        variant_ids: []
        width: number
    }>
    options: SelectProduct[]
    product_type: string
    published_at: string
    published_scope: string
    tags: string
    template_suffix: null
    title: string
    updated_at: string
    variants: any
    vendor: string
    selectedBrand: any,
    selectCategories: any
    selectedCountry: any
    selectedSize: any
    selectedColor: any
    manufacturerCode: string
    season: string,
    vendorCode: string
}
export interface Product {
    name: string,
    description: string,
    shopify_id: number,
    images: any,
    brand: string,
    category: string,
    country: string,
    manufacturer_code: string,
    vender_code: any,
    season: string,
    color_key: string,
    size_key: string,
    variants:Variants[]
}
export interface Variants {
    name: string
    stock: number
    price: string
    color: string  
    size: string
    shopify_id: number
    shopify_vendor_id: number 
}
export interface SelectProduct {
    id: number
    name: string
    position: number
    product_id: number
    values: Array<string>
}
export interface VendorInfo {
    email: string
    id: number
    login: string
    code: string
}